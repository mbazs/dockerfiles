set shiftwidth=4
set autoindent
set cindent
set expandtab
set tabstop=4
set wildmenu
set number
set syntax=on
filetype plugin indent on
syntax on 
set backspace=indent,eol,start
